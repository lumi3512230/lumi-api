# lumi-api

## Command to run API

`docker compose up --build`

## Command to configure db and populate with the invoices

`docker exec -it lumi-api sh /usr/src/lumi-api/scripts/configureDb.sh`
