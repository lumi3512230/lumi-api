FROM node:19.4.0-alpine

WORKDIR /usr/src/lumi-api

COPY . .

EXPOSE 5000

CMD ["npm", "run", "dev"]