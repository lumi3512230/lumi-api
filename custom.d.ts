// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { Request } from 'express';
import { FindOptions, Attributes } from 'sequelize';
import { Invoice } from 'src/db/models';

declare global {
	namespace Express {
		interface Request {
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
			options?: FindOptions<Attributes<Invoice>>;
		}
	}
}
