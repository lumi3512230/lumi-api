import { sequelize } from '../db/models/index';
export default function extractSequelizeOptionsFromQueryParams(req, res, next) {
	// eslint-disable-next-line @typescript-eslint/no-var-requires
	const searchBuilder = require('sequelize-search-builder');
	const search = new searchBuilder(sequelize, req.query);
	req.options = {
		where: search.getWhereQuery(),
		order: search.getOrderQuery(),
		limit: search.getLimitQuery(),
		offset: search.getOffsetQuery(),
	};
	next();
}
