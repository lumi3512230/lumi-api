import * as pdfjs from 'pdfjs-dist';
import { TextContent, TextItem } from 'pdfjs-dist/types/src/display/api';

export default class PdfHelpers {
	static async getContent(filePath: string): Promise<TextContent> {
		try {
			const doc = await pdfjs.getDocument(filePath).promise;
			const page = await doc.getPage(1);
			return page.getTextContent();
		} catch (err) {
			console.error(err);
			throw new Error('Error trying to read pdf content');
		}
	}

	static async getItems(src: string): Promise<TextItem[]> {
		return ((await PdfHelpers.getContent(src)).items as TextItem[]) || [];
	}

	static async getItemsStr(src: string): Promise<string[]> {
		const items = await PdfHelpers.getItems(src);
		return items.filter((i) => i.str != null).map((i) => i.str);
	}
}
