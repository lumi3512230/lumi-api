export function getFormattedBarcode(part1: string, part2: string, part3: string) {
	try {
		if (!part1 || part2 == null || part3 == null) return null;
		let barCode = part1 + part2 + part3;

		barCode = barCode.replace(/ /g, '');
		barCode = barCode.replace(/-/g, '');
		return barCode;
	} catch (err) {
		return null;
	}
}

export function convertStringToNumber(str: string) {
	try {
		if (!str) return NaN;
		str = str.trim();

		str = str.replace(/\./g, '');
		str = str.replace(',', '.');

		let isNegative = false;
		if (str.charAt(0) === '-') {
			isNegative = true;
			str = str.slice(1);
		}

		let number = parseFloat(str);
		if (isNegative) number = -number;
		return number;
	} catch (err) {
		return NaN;
	}
}

export function isNumeric(str: string) {
	return !Number.isNaN(convertStringToNumber(str));
}

export function convertMonthYearToDate(dataString: string) {
	const meses = {
		JAN: 0,
		FEV: 1,
		MAR: 2,
		ABR: 3,
		MAI: 4,
		JUN: 5,
		JUL: 6,
		AGO: 7,
		SET: 8,
		OUT: 9,
		NOV: 10,
		DEZ: 11,
	};

	const [mesStr, anoStr] = dataString.split('/');
	const mes = meses[mesStr.toUpperCase()];
	const ano = parseInt(anoStr?.length === 2 ? `20${anoStr}` : anoStr);
	if (Number.isNaN(mes) || Number.isNaN(ano)) return null;
	const data = new Date(ano, mes, 1);
	data.setMonth(data.getMonth() - 1);

	return data;
}

export function convertDayMonthYearToDate(dataString: string) {
	const [dia, mes, ano] = dataString.split('/');
	if (!dia || !mes || !ano) return null;
	const data = new Date(`${ano}-${mes}-${dia}`);
	return data;
}
