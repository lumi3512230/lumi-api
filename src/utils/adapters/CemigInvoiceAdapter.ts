import PdfHelpers from '../helpers/PdfHelpers';
import { convertStringToNumber, getFormattedBarcode, isNumeric } from '../utils';
import { ConsumptionHistory, InvoiceItem, Invoice } from './types';

export default class CemigInvoiceAdapter {
	private static getInvoiceItems(
		itemsStr: string[],
		titleIdx: number,
	): { invoiceItems: InvoiceItem[] } {
		try {
			const invoiceItems: InvoiceItem[] = [];
			let index = titleIdx + 27;
			while (itemsStr[index] != null && itemsStr[index].toLowerCase().trim() != 'total') {
				if (isNumeric(itemsStr[index + 2])) {
					const invoiceItem = {
						name: itemsStr[index].trim(),
						value: convertStringToNumber(itemsStr[index + 2]),
					};
					if (!invoiceItem.name || Number.isNaN(invoiceItem.value))
						throw new Error('Unexpected error getting invoice item values');

					invoiceItems.push(invoiceItem);
					itemsStr[index + 4].trim() ? (index += 4) : (index += 3);
				} else {
					const invoiceItem = {
						name: itemsStr[index].trim(),
						unit: itemsStr[index + 2].trim(),
						amount: convertStringToNumber(itemsStr[index + 4]),
						unitPrice: convertStringToNumber(itemsStr[index + 6]),
						value: convertStringToNumber(itemsStr[index + 8]),
						unitRate: convertStringToNumber(itemsStr[index + 10]),
					};
					if (
						!invoiceItem.name ||
						!invoiceItem.unit ||
						Number.isNaN(invoiceItem.amount) ||
						Number.isNaN(invoiceItem.unitPrice) ||
						Number.isNaN(invoiceItem.value) ||
						Number.isNaN(invoiceItem.unitPrice)
					)
						throw new Error('Unexpected error getting invoice item values');

					invoiceItems.push(invoiceItem);
					index += 11;
				}
			}

			return { invoiceItems };
		} catch (err) {
			console.error(err);
			return { invoiceItems: [] };
		}
	}

	private static getConsumptionHistory(
		itemsStr: string[],
		titleIdx: number,
	): { consumptionHistory: ConsumptionHistory[] } {
		try {
			const consumptionHistory: ConsumptionHistory[] = [];
			for (let i = 0; i < 12 + 1; i++) {
				const consumptionHistoryItem = {
					monthYear: itemsStr[titleIdx + 10 + 8 * i].trim(),
					consumptionKwh: convertStringToNumber(itemsStr[titleIdx + 12 + 8 * i]),
					averagePerDay: convertStringToNumber(itemsStr[titleIdx + 14 + 8 * i]),
					days: convertStringToNumber(itemsStr[titleIdx + 16 + 8 * i]),
				};
				if (
					!consumptionHistoryItem.monthYear ||
					Number.isNaN(consumptionHistoryItem.consumptionKwh) ||
					Number.isNaN(consumptionHistoryItem.averagePerDay) ||
					Number.isNaN(consumptionHistoryItem.days)
				)
					throw new Error('Unexpected error getting consumption history item values');
				consumptionHistory.push(consumptionHistoryItem);
			}
			return { consumptionHistory };
		} catch (err) {
			console.error(err);
			return { consumptionHistory: [] };
		}
	}

	private static getTechnicalInformation(itemsStr: string[], titleIdx: number) {
		try {
			const index = titleIdx + 19;
			const result = {
				measurementType: itemsStr[index].trim(),
				measurementCode: itemsStr[index + 2].trim(),
				previousReading: convertStringToNumber(itemsStr[index + 4]),
				currentReading: convertStringToNumber(itemsStr[index + 6]),
				multiplicationConstant: convertStringToNumber(itemsStr[index + 8]),
				consumptionKwh: convertStringToNumber(itemsStr[index + 10]),
			};
			if (
				!result.measurementType ||
				!result.measurementCode ||
				Number.isNaN(result.previousReading) ||
				Number.isNaN(result.currentReading) ||
				Number.isNaN(result.multiplicationConstant) ||
				Number.isNaN(result.consumptionKwh)
			)
				throw new Error('Unexpected error getting technical values');
			return result;
		} catch (err) {
			console.error(err);
			return {};
		}
	}

	private static getReservedForTaxAuthorities(itemsStr: string[], titleIdx: number) {
		try {
			const index = titleIdx + 2;
			if (!itemsStr[index].trim())
				throw new Error('Unexpected error getting reserved for tax authorities value');
			return {
				reserverdForTaxAuthorities: itemsStr[index].trim(),
			};
		} catch (err) {
			console.error(err);
			return {};
		}
	}

	private static getGeneralInformation(itemsStr: string[], titleIdx: number) {
		try {
			let index = titleIdx + 2;
			let generalInformation = '';
			while (itemsStr[index]?.trim()) {
				generalInformation = `${generalInformation}${itemsStr[index++]?.trim()}`;
			}
			if (!generalInformation)
				throw new Error('Unexpected error getting general information value');
			return { generalInformation };
		} catch (err) {
			console.error(err);
			return {};
		}
	}

	private static getInvoiceHeader(itemsStr: string[], titleIdx: number) {
		try {
			const result = {
				referringTo: itemsStr[titleIdx + 6]?.trim(),
				dueDate: itemsStr[titleIdx + 8]?.trim(),
				valueToPay: convertStringToNumber(itemsStr[titleIdx + 10]),
			};
			if (!result.referringTo || !result.dueDate || Number.isNaN(result.valueToPay))
				throw new Error('Unexpected error getting invoice header values');
			return result;
		} catch (err) {
			console.error(err);
			return {};
		}
	}

	private static getInvoiceFooter(itemsStr: string[], titleIdx: number) {
		try {
			const result = {
				automaticDebitCode: itemsStr[titleIdx + 8]?.trim(),
				barCode: getFormattedBarcode(
					itemsStr[titleIdx + 16],
					itemsStr[titleIdx + 17],
					itemsStr[titleIdx + 18],
				),
			};
			if (!result.automaticDebitCode)
				throw new Error('Unexpected error getting invoice footer values');
			return result;
		} catch (err) {
			console.error(err);
			return {};
		}
	}

	private static getClientNumbers(itemsStr: string[], titleIdx: number) {
		try {
			const result = {
				clientNumber: itemsStr[titleIdx + 4]?.trim(),
				installationNumber: itemsStr[titleIdx + 6]?.trim(),
			};
			if (!result.clientNumber || !result.installationNumber)
				throw new Error('Unexpected error getting client number values');
			return result;
		} catch (err) {
			console.error(err);
			return {};
		}
	}

	private static getTariffModality(itemsStr: string[], titleIdx: number) {
		try {
			const result = {
				class: itemsStr[titleIdx + 8]?.trim() + ' ' + itemsStr[titleIdx + 19]?.trim(),
				subclass: itemsStr[titleIdx + 10]?.trim() + ' ' + itemsStr[titleIdx + 21]?.trim(),
				tariffModality: itemsStr[titleIdx + 12]?.trim(),
				previousReadingDate: itemsStr[titleIdx + 23]?.trim(),
				currentReadingDate: itemsStr[titleIdx + 25]?.trim(),
				numberOfDaysBetweenReadings: convertStringToNumber(itemsStr[titleIdx + 27]),
				nextReadingDate: itemsStr[titleIdx + 29]?.trim(),
			};
			if (
				!result.class.trim() ||
				!result.subclass.trim() ||
				!result.tariffModality ||
				!result.previousReadingDate ||
				!result.currentReadingDate ||
				!result.numberOfDaysBetweenReadings ||
				!result.nextReadingDate
			)
				throw new Error('Unexpected error getting tariff modality values');
			return result;
		} catch (err) {
			console.error(err);
			return {};
		}
	}

	private static getNft3eInfos(itemsStr: string[], titleIdx: number) {
		try {
			const splittedTitle = itemsStr[titleIdx].split(' ');
			const hasAccessKey = splittedTitle[6].toLowerCase().trim() != 'u';
			const [, nft3eIssueDate] = itemsStr[hasAccessKey ? titleIdx + 1 : titleIdx + 2].split(': ');
			const [, nft3eaAthorizationProtocol] = itemsStr[titleIdx + 6].split(': ');

			const result = hasAccessKey
				? {
						nft3eNumber: splittedTitle[3],
						nft3eSeries: splittedTitle[6],
						nft3eIssueDate,
						nft3eAccessKey: itemsStr[titleIdx + 5].trim(),
						nft3eaAthorizationProtocol,
				  }
				: {
						nft3eNumber: splittedTitle[3],
						nft3eSeries: splittedTitle[6],
						nft3eIssueDate,
				  };

			if (
				!result.nft3eNumber ||
				!result.nft3eSeries ||
				!nft3eIssueDate ||
				(hasAccessKey && !(result.nft3eAccessKey && result.nft3eaAthorizationProtocol))
			)
				throw new Error('Unexpected error getting Nft3e infos');
			return result;
		} catch (err) {
			console.error(err);
			return {};
		}
	}

	private static getClientInfos(itemsStr: string[], titleIdx: number) {
		try {
			const result = {
				clientName: itemsStr[titleIdx - 5].trim(),
				clientAddress: `${itemsStr[titleIdx - 3].trim()}, ${itemsStr[
					titleIdx - 2
				].trim()}, ${itemsStr[titleIdx - 1].trim()}`,
				clientCPF: itemsStr[titleIdx].split(' ')[1],
			};
			if (!result.clientName || !result.clientAddress || !result.clientCPF)
				throw new Error('Unexpected error getting client infos');
			return result;
		} catch (err) {
			console.error(err);
			return {};
		}
	}

	static async getJsonFromPdf(filePath: string) {
		const itemsStr = await PdfHelpers.getItemsStr(filePath);

		if (!itemsStr.some((i) => i?.trim().toLowerCase().includes('cemig ')))
			throw new Error('Invalid invoice pdf. It must be a pdf from Cemig.');

		let invoice: Partial<Invoice> = {};

		itemsStr.forEach((itemStr, idx) => {
			invoice = {
				...invoice,
				...(itemStr?.toLowerCase().trim() == 'valores faturados'
					? CemigInvoiceAdapter.getInvoiceItems
					: itemStr?.toLowerCase().trim() == 'informações técnicas'
					? CemigInvoiceAdapter.getTechnicalInformation
					: itemStr?.toLowerCase().trim() == 'reservado ao fisco'
					? CemigInvoiceAdapter.getReservedForTaxAuthorities
					: itemStr?.toLowerCase().trim() == 'informações gerais'
					? CemigInvoiceAdapter.getGeneralInformation
					: itemStr?.toLowerCase().trim() == 'referente a'
					? CemigInvoiceAdapter.getInvoiceHeader
					: itemStr?.toLowerCase().trim() == 'nº do cliente'
					? CemigInvoiceAdapter.getClientNumbers
					: itemStr?.toLowerCase().trim() == 'classe'
					? CemigInvoiceAdapter.getTariffModality
					: itemStr?.toLowerCase().trim() == 'código de débito automático'
					? CemigInvoiceAdapter.getInvoiceFooter
					: itemStr?.toLowerCase().trim().includes('nota fiscal nº')
					? CemigInvoiceAdapter.getNft3eInfos
					: itemStr?.toLowerCase().trim().includes('cpf ')
					? CemigInvoiceAdapter.getClientInfos
					: itemStr?.toLowerCase().trim() == 'histórico de consumo'
					? CemigInvoiceAdapter.getConsumptionHistory
					: () => ({}))(itemsStr, idx),
			};
		});

		// check if the invoice have the required values
		if (
			!invoice.clientNumber ||
			!invoice.installationNumber ||
			!invoice.referringTo ||
			!invoice.dueDate ||
			Number.isNaN(invoice.valueToPay)
		)
			throw new Error(
				'Unexpected error reading pdf. This invoice pdf probably has a different pattern than expected.',
			);
		return invoice as Invoice;
	}
}
