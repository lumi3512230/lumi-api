export type Invoice = {
	// client info
	clientNumber: string;
	clientName?: string;
	clientAddress?: string;
	clientCPF?: string;

	// invoice header
	installationNumber: string;
	referringTo: string;
	dueDate: string;
	valueToPay: number;
	nft3eNumber?: string;
	nft3eSeries?: string;
	nft3eIssueDate?: string;
	nft3eAccessKey?: string;
	nft3eaAthorizationProtocol?: string;

	// class and tariff modality
	class?: string;
	subclass?: string;
	tariffModality?: string;
	previousReadingDate?: string;
	currentReadingDate?: string;
	numberOfDaysBetweenReadings?: number;
	nextReadingDate?: string;

	// technical information
	measurementType?: string;
	measurementCode?: string;
	previousReading?: number;
	currentReading?: number;
	multiplicationConstant?: number;
	consumptionKwh?: number;

	generalInformation?: string;
	reserverdForTaxAuthorities?: string;

	// invoice footer
	automaticDebitCode?: string;
	barCode?: string | null;

	consumptionHistory: ConsumptionHistory[];
	invoiceItems: InvoiceItem[];
};

export type ConsumptionHistory = {
	monthYear: string;
	consumptionKwh: number;
	averagePerDay: number;
	days: number;
};

export type InvoiceItem = {
	name: string;
	unit?: string | null;
	amount?: number | null;
	unitPrice?: number | null;
	value: number;
	unitRate?: number | null;
};
