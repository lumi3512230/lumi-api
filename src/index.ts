import * as dotenv from 'dotenv';
dotenv.config();
import app from './app';

const port = process.env.PORT;

app.listen(port, () => {
	console.log(`Lumi API Server listening on ${port}`);
});

process.on('SIGINT', function () {
	console.log('Terminating Lumi API Server...');
});
