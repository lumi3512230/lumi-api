import express, { Express, Request, Response } from 'express';

const app: Express = express();
app.use(express.json());

app.get('/', (req: Request, res: Response) => {
	res.send({
		api: {
			name: process.env.NAME || 'Lumi API',
			version: process.env.VERSION || '1.0.0',
		},
	});
});

import { invoiceRoutes } from './entities/invoice';
import { invoiceItemRoutes } from './entities/invoiceItem';
import { consumptionHistoryRoutes } from './entities/consumptionHistory';

app.use('/invoices', invoiceRoutes);
app.use('/invoiceItems', invoiceItemRoutes);
app.use('/comsumptionHistories', consumptionHistoryRoutes);

export default app;
