import * as fs from 'fs';
import { Migration } from 'sequelize-cli';
import { convertDayMonthYearToDate, convertMonthYearToDate } from '../../utils/utils';
import CemigInvoiceAdapter from '../../utils/adapters/CemigInvoiceAdapter';
import { consumptionHistoryDao } from '../../entities/consumptionHistory';

const Invoices = 'Invoices';
const InvoiceItems = 'InvoiceItems';
const ConsumptionHistories = 'ConsumptionHistories';

const seeders: Migration = {
	async up(queryInterface) {
		await queryInterface.bulkDelete(InvoiceItems, {});
		await queryInterface.bulkDelete(ConsumptionHistories, {});
		await queryInterface.bulkDelete(Invoices, {});

		const INVOICES_DIR = '/usr/src/lumi-api/invoices/';

		const fileNames = fs.readdirSync(INVOICES_DIR);
		for (let i = 0; i < fileNames.length; i++) {
			const { consumptionHistory, invoiceItems, ...invoice } =
				await CemigInvoiceAdapter.getJsonFromPdf(INVOICES_DIR + fileNames[i]);

			await queryInterface.bulkInsert(Invoices, [
				{
					id: i,
					...invoice,
					referringToRaw: invoice.referringTo,
					referringTo: convertMonthYearToDate(invoice.referringTo),
					dueDateRaw: invoice.dueDate,
					dueDate: convertDayMonthYearToDate(invoice.dueDate),
					createdAt: new Date(),
					updatedAt: new Date(),
				},
			]);
			await queryInterface.bulkInsert(
				InvoiceItems,
				invoiceItems.map((item) => ({
					invoiceId: i,
					...item,
					createdAt: new Date(),
					updatedAt: new Date(),
				})),
			);

			for (const c of consumptionHistory) {
				const found = await consumptionHistoryDao.findOne({
					where: {
						installationNumber: invoice.installationNumber,
						monthYearRaw: c.monthYear,
					},
				});
				if (found) continue;
				consumptionHistoryDao.create({
					installationNumber: invoice.installationNumber,
					...c,
					monthYearRaw: c.monthYear,
					monthYear: convertMonthYearToDate(c.monthYear),
				});
			}
		}
	},

	async down(queryInterface) {
		await queryInterface.bulkDelete(InvoiceItems, {});
		await queryInterface.bulkDelete(ConsumptionHistories, {});
		await queryInterface.bulkDelete(Invoices, {});
	},
};

export default seeders;
