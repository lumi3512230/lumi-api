import { sequelize } from '.';
import { InvoiceItem } from './InvoiceItem';

import {
	Model,
	InferAttributes,
	InferCreationAttributes,
	CreationOptional,
	DataTypes,
	NonAttribute,
	Association,
} from 'sequelize';

export class Invoice extends Model<InferAttributes<Invoice>, InferCreationAttributes<Invoice>> {
	declare id: CreationOptional<number>;

	// client info
	declare clientNumber: string;
	declare clientName?: string | null;
	declare clientAddress?: string | null;
	declare clientCPF?: string | null;

	// invoice header
	declare installationNumber: string;
	declare referringToRaw: string;
	declare referringTo: Date;
	declare dueDateRaw: string;
	declare dueDate: Date;
	declare valueToPay: number;
	declare nft3eNumber?: string | null;
	declare nft3eSeries?: string | null;
	declare nft3eIssueDate?: string | null;
	declare nft3eAccessKey?: string | null;
	declare nft3eaAthorizationProtocol?: string | null;

	// class and tariff modality
	declare class?: string | null;
	declare subclass?: string | null;
	declare tariffModality?: string | null;
	declare previousReadingDate?: string | null;
	declare currentReadingDate?: string | null;
	declare numberOfDaysBetweenReadings?: number | null;
	declare nextReadingDate?: string | null;

	// technical information
	declare measurementType?: string | null;
	declare measurementCode?: string | null;
	declare previousReading?: number | null;
	declare currentReading?: number | null;
	declare multiplicationConstant?: number | null;
	declare consumptionKwh?: number | null;

	declare generalInformation?: string | null;
	declare reserverdForTaxAuthorities?: string | null;

	// invoice footer
	declare automaticDebitCode?: string | null;
	declare barCode?: string | null;

	declare createdAt: CreationOptional<Date>;
	declare updatedAt: CreationOptional<Date>;
	declare deletedAt: CreationOptional<Date | null>;

	declare items?: NonAttribute<InvoiceItem>;

	declare static associations: {
		items: Association<Invoice, InvoiceItem>;
	};
}

Invoice.init(
	{
		id: {
			primaryKey: true,
			type: DataTypes.INTEGER,
			autoIncrement: true,
			allowNull: false,
			unique: true,
		},
		clientNumber: {
			type: DataTypes.STRING(20),
			allowNull: false,
		},
		installationNumber: {
			type: DataTypes.STRING(20),
			allowNull: false,
		},
		referringToRaw: {
			type: DataTypes.STRING(10),
			allowNull: false,
		},
		referringTo: {
			type: DataTypes.DATEONLY,
			allowNull: false,
		},
		dueDateRaw: {
			type: DataTypes.STRING(20),
			allowNull: false,
		},
		dueDate: {
			type: DataTypes.DATEONLY,
			allowNull: false,
		},
		valueToPay: {
			type: DataTypes.DECIMAL(10, 2),
			allowNull: false,
		},
		clientName: {
			type: DataTypes.STRING(100),
			allowNull: true,
		},
		clientAddress: {
			type: DataTypes.STRING,
			allowNull: true,
		},
		clientCPF: {
			type: DataTypes.STRING(20),
			allowNull: true,
		},
		nft3eNumber: {
			type: DataTypes.STRING(20),
			allowNull: true,
		},
		nft3eSeries: {
			type: DataTypes.STRING(3),
			allowNull: true,
		},
		nft3eIssueDate: {
			type: DataTypes.STRING(20),
			allowNull: true,
		},
		nft3eAccessKey: {
			type: DataTypes.STRING(50),
			allowNull: true,
		},
		nft3eaAthorizationProtocol: {
			type: DataTypes.STRING(20),
			allowNull: true,
		},
		class: {
			type: DataTypes.STRING(50),
			allowNull: true,
		},
		subclass: {
			type: DataTypes.STRING(50),
			allowNull: true,
		},
		tariffModality: {
			type: DataTypes.STRING(50),
			allowNull: true,
		},
		previousReadingDate: {
			type: DataTypes.CHAR(5),
			allowNull: true,
		},
		currentReadingDate: {
			type: DataTypes.CHAR(5),
			allowNull: true,
		},
		numberOfDaysBetweenReadings: {
			type: DataTypes.INTEGER,
			allowNull: true,
		},
		nextReadingDate: {
			type: DataTypes.CHAR(5),
			allowNull: true,
		},
		measurementType: {
			type: DataTypes.STRING(20),
			allowNull: true,
		},
		measurementCode: {
			type: DataTypes.STRING(25),
			allowNull: true,
		},
		previousReading: {
			type: DataTypes.INTEGER,
			allowNull: true,
		},
		currentReading: {
			type: DataTypes.INTEGER,
			allowNull: true,
		},
		multiplicationConstant: {
			type: DataTypes.INTEGER,
			allowNull: true,
		},
		consumptionKwh: {
			type: DataTypes.INTEGER,
			allowNull: true,
		},
		generalInformation: {
			type: DataTypes.STRING(1000),
			allowNull: true,
		},
		reserverdForTaxAuthorities: {
			type: DataTypes.STRING(20),
			allowNull: true,
		},
		automaticDebitCode: {
			type: DataTypes.STRING(20),
			allowNull: true,
		},
		barCode: {
			type: DataTypes.CHAR(48),
			allowNull: true,
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: false,
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
		},
		deletedAt: {
			type: DataTypes.DATE,
			allowNull: true,
		},
	},
	{
		sequelize,
		modelName: 'Invoice',
		tableName: 'Invoices',
		paranoid: true,
	},
);

Invoice.hasMany(InvoiceItem, { foreignKey: 'invoiceId', as: 'items' });
