import { sequelize } from '.';
import {
	Model,
	InferAttributes,
	InferCreationAttributes,
	CreationOptional,
	DataTypes,
} from 'sequelize';

export class ConsumptionHistory extends Model<
	InferAttributes<ConsumptionHistory>,
	InferCreationAttributes<ConsumptionHistory>
> {
	declare id: CreationOptional<number>;

	declare installationNumber: string;
	declare monthYearRaw: string;
	declare monthYear: Date | null;
	declare consumptionKwh: number;
	declare averagePerDay: number;
	declare days: number;

	declare createdAt: CreationOptional<Date>;
	declare updatedAt: CreationOptional<Date>;
	declare deletedAt: CreationOptional<Date | null>;
}

ConsumptionHistory.init(
	{
		id: {
			primaryKey: true,
			type: DataTypes.INTEGER,
			autoIncrement: true,
			allowNull: false,
			unique: true,
		},
		installationNumber: {
			type: DataTypes.STRING(20),
			allowNull: false,
		},
		monthYearRaw: {
			type: DataTypes.CHAR(6),
			allowNull: false,
		},
		monthYear: {
			type: DataTypes.DATEONLY,
			allowNull: false,
		},
		consumptionKwh: {
			type: DataTypes.INTEGER,
			allowNull: false,
		},
		averagePerDay: {
			type: DataTypes.DECIMAL(7, 2),
			allowNull: false,
		},
		days: {
			type: DataTypes.INTEGER,
			allowNull: false,
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: false,
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
		},
		deletedAt: {
			type: DataTypes.DATE,
			allowNull: true,
		},
	},
	{
		sequelize,
		modelName: 'ConsumptionHistory',
		tableName: 'ConsumptionHistories',
		paranoid: true,
	},
);
