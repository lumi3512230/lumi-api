import { sequelize } from '.';
import {
	Model,
	InferAttributes,
	InferCreationAttributes,
	CreationOptional,
	DataTypes,
} from 'sequelize';
import { Invoice } from './Invoice';

export class InvoiceItem extends Model<
	InferAttributes<InvoiceItem>,
	InferCreationAttributes<InvoiceItem>
> {
	declare id: CreationOptional<number>;

	declare invoiceId: number;
	declare name: string;
	declare value: number;
	declare unit?: string | null;
	declare amount?: number | null;
	declare unitPrice?: number | null;
	declare unitRate?: number | null;

	declare createdAt: CreationOptional<Date>;
	declare updatedAt: CreationOptional<Date>;
	declare deletedAt: CreationOptional<Date | null>;
}

InvoiceItem.init(
	{
		id: {
			primaryKey: true,
			type: DataTypes.INTEGER,
			autoIncrement: true,
			allowNull: false,
			unique: true,
		},
		invoiceId: {
			type: DataTypes.INTEGER,
			references: {
				model: Invoice,
				key: 'id',
			},
			allowNull: false,
		},
		name: {
			type: DataTypes.STRING(50),
			allowNull: false,
		},
		value: {
			type: DataTypes.DECIMAL(10, 2),
			allowNull: false,
		},
		unit: {
			type: DataTypes.STRING(10),
			allowNull: true,
		},
		amount: {
			type: DataTypes.INTEGER,
			allowNull: true,
		},
		unitPrice: {
			type: DataTypes.DECIMAL(9, 8),
			allowNull: true,
		},
		unitRate: {
			type: DataTypes.DECIMAL(9, 8),
			allowNull: true,
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: false,
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
		},
		deletedAt: {
			type: DataTypes.DATE,
			allowNull: true,
		},
	},
	{
		sequelize,
		modelName: 'InvoiceItem',
		tableName: 'InvoiceItems',
		paranoid: true,
	},
);
