/* eslint-disable @typescript-eslint/no-var-requires */
import { Sequelize } from 'sequelize';
const configsByEnv = require('../config/config');
const env = process.env.NODE_ENV || 'development';
const config = configsByEnv[env];

export let sequelize: Sequelize;
if (config.use_env_variable && process.env[config.use_env_variable]) {
	sequelize = new Sequelize(process.env[config.use_env_variable] as string, config);
} else {
	sequelize = new Sequelize(config.database, config.username, config.password, config);
}

export { Sequelize } from 'sequelize';

export { Invoice } from './Invoice';
export { InvoiceItem } from './InvoiceItem';
export { ConsumptionHistory } from './ConsumptionHistory';
