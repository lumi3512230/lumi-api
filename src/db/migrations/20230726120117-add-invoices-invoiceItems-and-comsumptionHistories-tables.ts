import { Migration } from 'sequelize-cli';

const Invoices = 'Invoices';
const InvoiceItems = 'InvoiceItems';
const ConsumptionHistories = 'ConsumptionHistories';

const migration: Migration = {
	async up(queryInterface, Sequelize) {
		return queryInterface.sequelize.transaction(async (transaction) => {
			await Promise.all([
				queryInterface.createTable(
					Invoices,
					{
						id: {
							primaryKey: true,
							type: Sequelize.INTEGER,
							autoIncrement: true,
							allowNull: false,
							unique: true,
						},
						clientNumber: {
							type: Sequelize.STRING(20),
							allowNull: false,
						},
						installationNumber: {
							type: Sequelize.STRING(20),
							allowNull: false,
						},
						referringToRaw: {
							type: Sequelize.STRING(10),
							allowNull: false,
						},
						referringTo: {
							type: Sequelize.DATEONLY,
							allowNull: false,
						},
						dueDateRaw: {
							type: Sequelize.STRING(20),
							allowNull: false,
						},
						dueDate: {
							type: Sequelize.DATEONLY,
							allowNull: false,
						},
						valueToPay: {
							type: Sequelize.DECIMAL(10, 2),
							allowNull: false,
						},
						clientName: {
							type: Sequelize.STRING(100),
							allowNull: true,
						},
						clientAddress: {
							type: Sequelize.STRING,
							allowNull: true,
						},
						clientCPF: {
							type: Sequelize.STRING(20),
							allowNull: true,
						},
						nft3eNumber: {
							type: Sequelize.STRING(20),
							allowNull: true,
						},
						nft3eSeries: {
							type: Sequelize.STRING(3),
							allowNull: true,
						},
						nft3eIssueDate: {
							type: Sequelize.STRING(20),
							allowNull: true,
						},
						nft3eAccessKey: {
							type: Sequelize.STRING(50),
							allowNull: true,
						},
						nft3eaAthorizationProtocol: {
							type: Sequelize.STRING(20),
							allowNull: true,
						},
						class: {
							type: Sequelize.STRING(50),
							allowNull: true,
						},
						subclass: {
							type: Sequelize.STRING(50),
							allowNull: true,
						},
						tariffModality: {
							type: Sequelize.STRING(50),
							allowNull: true,
						},
						previousReadingDate: {
							type: Sequelize.CHAR(5),
							allowNull: true,
						},
						currentReadingDate: {
							type: Sequelize.CHAR(5),
							allowNull: true,
						},
						numberOfDaysBetweenReadings: {
							type: Sequelize.INTEGER,
							allowNull: true,
						},
						nextReadingDate: {
							type: Sequelize.CHAR(5),
							allowNull: true,
						},
						measurementType: {
							type: Sequelize.STRING(20),
							allowNull: true,
						},
						measurementCode: {
							type: Sequelize.STRING(25),
							allowNull: true,
						},
						previousReading: {
							type: Sequelize.INTEGER,
							allowNull: true,
						},
						currentReading: {
							type: Sequelize.INTEGER,
							allowNull: true,
						},
						multiplicationConstant: {
							type: Sequelize.INTEGER,
							allowNull: true,
						},
						consumptionKwh: {
							type: Sequelize.INTEGER,
							allowNull: true,
						},
						generalInformation: {
							type: Sequelize.STRING(1000),
							allowNull: true,
						},
						reserverdForTaxAuthorities: {
							type: Sequelize.STRING(20),
							allowNull: true,
						},
						automaticDebitCode: {
							type: Sequelize.STRING(20),
							allowNull: true,
						},
						barCode: {
							type: Sequelize.CHAR(48),
							allowNull: true,
						},
						createdAt: {
							type: Sequelize.DATE,
							allowNull: false,
						},
						updatedAt: {
							type: Sequelize.DATE,
							allowNull: false,
						},
						deletedAt: {
							type: Sequelize.DATE,
							allowNull: true,
						},
					},
					{ transaction },
				),
				queryInterface.createTable(
					InvoiceItems,
					{
						id: {
							primaryKey: true,
							type: Sequelize.INTEGER,
							autoIncrement: true,
							allowNull: false,
							unique: true,
						},
						invoiceId: {
							type: Sequelize.INTEGER,
							references: {
								model: Invoices,
								key: 'id',
							},
							allowNull: false,
						},
						name: {
							type: Sequelize.STRING(50),
							allowNull: false,
						},
						value: {
							type: Sequelize.DECIMAL(10, 2),
							allowNull: false,
						},
						unit: {
							type: Sequelize.STRING(10),
							allowNull: true,
						},
						amount: {
							type: Sequelize.INTEGER,
							allowNull: true,
						},
						unitPrice: {
							type: Sequelize.DECIMAL(9, 8),
							allowNull: true,
						},
						unitRate: {
							type: Sequelize.DECIMAL(9, 8),
							allowNull: true,
						},
						createdAt: {
							type: Sequelize.DATE,
							allowNull: false,
						},
						updatedAt: {
							type: Sequelize.DATE,
							allowNull: false,
						},
						deletedAt: {
							type: Sequelize.DATE,
							allowNull: true,
						},
					},
					{ transaction },
				),
				queryInterface.createTable(
					ConsumptionHistories,
					{
						id: {
							primaryKey: true,
							type: Sequelize.INTEGER,
							autoIncrement: true,
							allowNull: false,
							unique: true,
						},
						installationNumber: {
							type: Sequelize.STRING(20),
							allowNull: false,
						},
						monthYearRaw: {
							type: Sequelize.CHAR(6),
							allowNull: false,
						},
						monthYear: {
							type: Sequelize.DATEONLY,
							allowNull: false,
						},
						consumptionKwh: {
							type: Sequelize.INTEGER,
							allowNull: false,
						},
						averagePerDay: {
							type: Sequelize.DECIMAL(7, 2),
							allowNull: false,
						},
						days: {
							type: Sequelize.INTEGER,
							allowNull: false,
						},
						createdAt: {
							type: Sequelize.DATE,
							allowNull: false,
						},
						updatedAt: {
							type: Sequelize.DATE,
							allowNull: false,
						},
						deletedAt: {
							type: Sequelize.DATE,
							allowNull: true,
						},
					},
					{ transaction },
				),
			]);
		});
	},

	async down(queryInterface) {
		return queryInterface.sequelize.transaction(async (transaction) => {
			await Promise.all([
				queryInterface.dropTable(InvoiceItems, { transaction }),
				queryInterface.dropTable(ConsumptionHistories, { transaction }),
				queryInterface.dropTable(Invoices, { transaction }),
			]);
		});
	},
};

export default migration;
