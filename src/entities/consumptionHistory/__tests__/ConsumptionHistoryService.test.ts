import { describe, expect, test } from '@jest/globals';
import { ConsumptionHistoryService } from '..';

describe('Comsumption history service module', () => {
	test('adds 1 + 2 to equal 3', async () => {
		const consumptionHistoryService = ConsumptionHistoryService.default();
		const result = await consumptionHistoryService.sum(1, 2);
		expect(result).toBe(3);
	});
});
