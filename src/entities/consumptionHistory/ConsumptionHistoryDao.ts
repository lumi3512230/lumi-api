import { BaseDao } from '../base';

import { ConsumptionHistory } from '../../db/models';

export default class ConsumptionHistoryDao extends BaseDao<
	ConsumptionHistory,
	typeof ConsumptionHistory
> {
	constructor() {
		super(ConsumptionHistory);
	}
}
