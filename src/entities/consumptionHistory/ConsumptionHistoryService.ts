import { BaseService } from '../base';

import { ConsumptionHistory } from '../../db/models';
import { ConsumptionHistoryDao } from '.';

export default class ConsumptionHistoryService extends BaseService<
	ConsumptionHistory,
	typeof ConsumptionHistory
> {
	static default() {
		return new ConsumptionHistoryService(new ConsumptionHistoryDao());
	}
	constructor(dao: ConsumptionHistoryDao) {
		super(dao);
	}

	// This method is just a mock for runing jest tests
	async sum(a: number, b: number): Promise<number> {
		return Promise.resolve(a + b);
	}
}
