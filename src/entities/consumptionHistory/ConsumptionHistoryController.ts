import { BaseController } from '../base';
import { ConsumptionHistory } from '../../db/models';

// import { Request, Response, NextFunction } from 'express';
import { ConsumptionHistoryService } from '.';

export default class ConsumptionHistoryController extends BaseController<
	ConsumptionHistory,
	typeof ConsumptionHistory
> {
	static default() {
		return new ConsumptionHistoryController(ConsumptionHistoryService.default());
	}
	constructor(service: ConsumptionHistoryService) {
		super(service, ConsumptionHistoryController);
	}
}
