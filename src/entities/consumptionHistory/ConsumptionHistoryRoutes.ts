import express from 'express';
import { ConsumptionHistoryController } from '.';

const router = express.Router();
const consumptionHistoryController = ConsumptionHistoryController.default();

router.post('/', consumptionHistoryController.create);
router.get('/', consumptionHistoryController.getAll);
router.get('/:id', consumptionHistoryController.getById);
router.put('/:id', consumptionHistoryController.updateById);
router.delete('/:id', consumptionHistoryController.deleteById);

export default router;
