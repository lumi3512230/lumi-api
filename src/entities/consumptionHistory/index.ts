import ConsumptionHistoryController from './ConsumptionHistoryController';
import ConsumptionHistoryService from './ConsumptionHistoryService';
import ConsumptionHistoryDao from './ConsumptionHistoryDao';
import consumptionHistoryRoutes from './ConsumptionHistoryRoutes';

export const consumptionHistoryDao = new ConsumptionHistoryDao();

export {
	ConsumptionHistoryController,
	ConsumptionHistoryService,
	ConsumptionHistoryDao,
	consumptionHistoryRoutes,
};
