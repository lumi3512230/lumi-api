import { BaseService } from '../base';

import { InvoiceItem } from '../../db/models';
import { InvoiceItemDao } from '.';

export default class InvoiceItemService extends BaseService<InvoiceItem, typeof InvoiceItem> {
	static default() {
		return new InvoiceItemService(new InvoiceItemDao());
	}
	constructor(dao: InvoiceItemDao) {
		super(dao);
	}

	// This method is just a mock for runing jest tests
	async sum(a: number, b: number): Promise<number> {
		return Promise.resolve(a + b);
	}
}
