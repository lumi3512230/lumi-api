import { BaseDao } from '../base';

import { InvoiceItem } from '../../db/models';

export default class InvoiceItemDao extends BaseDao<InvoiceItem, typeof InvoiceItem> {
	constructor() {
		super(InvoiceItem);
	}
}
