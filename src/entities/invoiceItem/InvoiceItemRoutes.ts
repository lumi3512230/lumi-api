import express from 'express';
import { InvoiceItemController } from '.';

const router = express.Router();
const invoiceItemController = InvoiceItemController.default();

router.post('/', invoiceItemController.create);
router.get('/', invoiceItemController.getAll);
router.get('/:id', invoiceItemController.getById);
router.put('/:id', invoiceItemController.updateById);
router.delete('/:id', invoiceItemController.deleteById);

export default router;
