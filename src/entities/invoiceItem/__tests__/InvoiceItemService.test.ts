import { describe, expect, test } from '@jest/globals';
import { InvoiceItemService } from '..';

describe('Invoice item service module', () => {
	test('adds 1 + 2 to equal 3', async () => {
		const invoiceItemService = InvoiceItemService.default();
		const result = await invoiceItemService.sum(1, 2);
		expect(result).toBe(3);
	});
});
