import { BaseController } from '../base';
import { InvoiceItem } from '../../db/models';

// import { Request, Response, NextFunction } from 'express';
import { InvoiceItemService } from '.';

export default class InvoiceItemController extends BaseController<InvoiceItem, typeof InvoiceItem> {
	static default() {
		return new InvoiceItemController(InvoiceItemService.default());
	}
	constructor(service: InvoiceItemService) {
		super(service, InvoiceItemController);
	}
}
