import express from 'express';
import { InvoiceController } from '.';
// eslint-disable-next-line @typescript-eslint/no-var-requires
import extractSequelizeOptionsFromQueryParams from '../../middlewares/searchBuilder';

const router = express.Router();
const invoiceController = InvoiceController.default();

router.get('/', extractSequelizeOptionsFromQueryParams, invoiceController.list);

export default router;
