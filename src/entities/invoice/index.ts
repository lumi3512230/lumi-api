import InvoiceController from './InvoiceController';
import InvoiceService from './InvoiceService';
import InvoiceDao from './InvoiceDao';
import invoiceRoutes from './InvoiceRoutes';

export const invoiceDao = new InvoiceDao();

export { InvoiceController, InvoiceService, InvoiceDao, invoiceRoutes };
