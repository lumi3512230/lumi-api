import { BaseController } from '../base';
import { Invoice } from '../../db/models';

import { Request, Response } from 'express';
import { InvoiceService } from '.';

export default class InvoiceController extends BaseController<Invoice, typeof Invoice> {
	static default() {
		return new InvoiceController(InvoiceService.default());
	}
	constructor(readonly service: InvoiceService) {
		super(service, InvoiceController);
	}
	async list(req: Request, res: Response) {
		return this.service
			.list(req.options ?? {})
			.then((data) => res.status(200).send(data))
			.catch((err) => res.status(400).send(err));
	}
}
