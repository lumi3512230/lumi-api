import { BaseDao } from '../base';

import { Invoice } from '../../db/models';

export default class InvoiceDao extends BaseDao<Invoice, typeof Invoice> {
	constructor() {
		super(Invoice);
	}
}
