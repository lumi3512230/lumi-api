import { BaseService } from '../base';

import { Invoice, InvoiceItem } from '../../db/models';
import { InvoiceDao } from '.';
import { FindOptions, Attributes } from 'sequelize';
import { consumptionHistoryDao } from '../consumptionHistory/index';

export default class InvoiceService extends BaseService<Invoice, typeof Invoice> {
	static default() {
		return new InvoiceService(new InvoiceDao());
	}
	constructor(readonly dao: InvoiceDao) {
		super(dao);
	}

	async list(options: FindOptions<Attributes<Invoice>>) {
		try {
			const invoices: Attributes<Invoice>[] = await this.dao
				.findAll({
					...options,
					include: [
						{
							model: InvoiceItem,
							as: 'items',
						},
					],
				})
				.then((invoices) => invoices.map((i) => i.get({ plain: true })));
			const consumptionHistories = await consumptionHistoryDao.findAll({
				where: { installationNumber: invoices.map((i) => i.installationNumber) },
			});
			new Date(new Date().setFullYear(new Date().getFullYear() - 1));
			return invoices.map((i) => ({
				...i,
				consumptionHistory: consumptionHistories.filter((c) => {
					const startDate = new Date(
						new Date(i.referringTo).setFullYear(new Date(i.referringTo).getFullYear() - 1),
					);
					const endDate = new Date(i.referringTo);
					const consumptionYear = c.monthYear ? new Date(c.monthYear) : new Date();

					return (
						c.installationNumber === i.installationNumber &&
						startDate <= consumptionYear &&
						consumptionYear <= endDate
					);
				}),
			}));
		} catch (err) {
			console.error(err);
			return null;
		}
	}
}
