import { describe, expect, test } from '@jest/globals';

describe('Invoice service module', () => {
	test('adds 1 + 2 to equal 3', async () => {
		expect(1 + 2).toBe(3);
	});
});
