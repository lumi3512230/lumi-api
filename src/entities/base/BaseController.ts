/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { BaseService } from '.';
import { Request, Response, NextFunction } from 'express';
import { Model, ModelStatic } from 'sequelize';

export default class BaseController<
	M extends Model<{ id?: number } | any, any>,
	T extends ModelStatic<M>,
> {
	constructor(
		protected readonly service: BaseService<M, T>,
		innerController: typeof BaseController<M, T>,
	) {
		// bind all controller methods
		[
			...Object.getOwnPropertyNames(BaseController.prototype),
			...Object.getOwnPropertyNames(innerController.prototype),
		].forEach((key: string) => {
			if (key !== 'constructor') {
				this[key] = this[key].bind(this);
			}
		});
	}

	async create(req: Request, res: Response, next: NextFunction) {
		return this.service
			.create(req.body)
			.then((data) => res.status(200).send(data))
			.catch((err) => res.status(400).send(err));
	}

	async getAll(req: Request, res: Response, next: NextFunction) {
		return this.service
			.getAll()
			.then((entity) => res.status(200).json(entity))
			.catch((err) => res.status(400).send(err));
	}

	async getById(req: Request, res: Response, next: NextFunction) {
		return this.service
			.getById(Number(req.params.id))
			.then((entity) => res.status(200).json(entity))
			.catch((err) => res.status(400).send(err));
	}

	async updateById(req: Request, res: Response, next: NextFunction) {
		return this.service
			.updateById(Number(req.params.id), req.body)
			.then(() => this.service.getById(Number(req.params.id)))
			.then((entity) => res.status(200).json(entity))
			.catch((err) => res.status(400).send(err));
	}

	async deleteById(req: Request, res: Response, next: NextFunction) {
		return this.service
			.deleteById(Number(req.params.id))
			.then((deleted) => res.status(200).send({ success: !!deleted }))
			.catch((err) => res.status(400).send(err));
	}
}
