/* eslint-disable @typescript-eslint/no-explicit-any */
import {
	Attributes,
	CreateOptions,
	DestroyOptions,
	FindOptions,
	Model,
	ModelStatic,
	UpdateOptions,
} from 'sequelize';
import { MakeNullishOptional } from 'sequelize/types/utils';

export default class BaseDao<
	M extends Model<{ id?: number } | any, any>,
	T extends ModelStatic<M>,
> {
	constructor(protected readonly model: T) {}

	async create(
		data: MakeNullishOptional<M['_creationAttributes']>,
		options?: CreateOptions<Attributes<M> | { id?: number }>,
	) {
		return this.model.create(data, options);
	}

	async findAll(options?: FindOptions<Attributes<M> | { id?: number }>) {
		return this.model.findAll(options);
	}

	async findOne(options?: FindOptions<Attributes<M> | { id?: number }>) {
		return this.model.findOne(options);
	}

	async update(
		data: Record<string, never>,
		options: UpdateOptions<Attributes<M> | { id?: number }>,
	) {
		return this.model.update(data, options);
	}

	async destroy(options?: DestroyOptions<Attributes<M> | { id?: number }>) {
		return this.model.destroy(options);
	}
}
