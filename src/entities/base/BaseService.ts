/* eslint-disable @typescript-eslint/no-explicit-any */
import { BaseDao } from '.';
import { Model, ModelStatic, Transaction } from 'sequelize';
import { MakeNullishOptional } from 'sequelize/types/utils';

export default class BaseService<
	M extends Model<{ id?: number } | any, any>,
	T extends ModelStatic<M>,
> {
	constructor(protected readonly dao: BaseDao<M, T>) {}

	async create(data: MakeNullishOptional<M['_creationAttributes']>, transaction?: Transaction) {
		return this.dao.create(data, { transaction });
	}

	async getAll(transaction?: Transaction) {
		return this.dao.findAll({ transaction });
	}

	async getById(id: number, transaction?: Transaction) {
		return this.dao.findOne({ where: { id }, transaction });
	}

	async updateById(id: number, data: Record<string, never>, transaction?: Transaction) {
		return this.dao.update(data, { where: { id }, transaction });
	}

	async deleteById(id: number, transaction?: Transaction) {
		return this.dao.destroy({ where: { id }, transaction });
	}
}
